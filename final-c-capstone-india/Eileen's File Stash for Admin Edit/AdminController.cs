﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using c_final_capstone_v2.Models;
using c_final_capstone_v2.DAL;
using System.Configuration;

namespace c_final_capstone_v2.Controllers
{
    public class AdminController : Controller
    {
        private ClientSqlDAL clientDAL;
        private MessagesSqlDAL messagesDAL;
        private CoachSqlDAL coachDAL;
        private FeedbackSqlDAL feedbackDAL;
        private string connectionString;

        public AdminController()
        {
            connectionString = ConfigurationManager.ConnectionStrings["MHMConnectionString"].ConnectionString;
            clientDAL = new ClientSqlDAL(connectionString);
            messagesDAL = new MessagesSqlDAL(connectionString);
            coachDAL = new CoachSqlDAL(connectionString);
            feedbackDAL = new FeedbackSqlDAL(connectionString);
        }

        // List of all coaches; will be later restricted to admin
        public ActionResult AdminCoachList()
        {
            List<Coach> coach = coachDAL.GetAllCoaches();
            return View("AdminCoachList", coach);
        }

        // Brings up specific coach profile page by Coach ID from DAL; will later be restricted to admin or called as subView for admin
        public ActionResult AdminCoachProfile(int id)
        {
            Coach coach = coachDAL.GetCoachDetails(id);

            return View("AdminCoachProfile", coach);
        }

        // Goes to Coach edit profile screen for Admin selected by ID in DAL
        public ActionResult AdminEditCoach(int id)
        {
            Coach coach = coachDAL.GetCoachDetails(id);
            return View("AdminEditCoach", coach);
        }

        // Sends update statement to database upon ADMIN editing information
        [HttpPost]
        public ActionResult AdminEditCoach(Coach coach)
        {
              
            if (!ModelState.IsValid)
            {
                return View("AdminEditCoach", coach);
            }

            coachDAL.AdminUpdateCoach(coach);
            return RedirectToAction("AdminCoachProfile", "Admin", new { id = coach.CoachId });
        }

        // Generate form for admin to add a new coach
        [HttpGet]
        public ActionResult AddNewCoach()
        {
            return View("AddNewCoach");
        }

        //Submitting a Completed Coach Profile Form
        [HttpPost]
        public ActionResult AddNewCoach(Coach completedCoachProfile)
        {
            if (!ModelState.IsValid)
            {
                return View("AddNewCoach", completedCoachProfile);
            }
            else
            {
                coachDAL.AdminAddCoach(completedCoachProfile);
                return RedirectToAction("AdminCoachList", "Admin");
            }
        }

        //Deletes a coach from the database, directly from their profile
        public ActionResult DeleteCoach(Coach coach)
        {
            coachDAL.AdminRemoveCoach(coach);
            return View("AdminCoachList", "Admin");
        }
    }
}
