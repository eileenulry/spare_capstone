﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using c_final_capstone_v2.Models;
using c_final_capstone_v2.DAL;

namespace c_final_capstone_v2.Controllers
{
    public class HomeController : Controller
    {
        private ClientSqlDAL clientDAL;
        private MessagesSqlDAL messagesDAL;
        private CoachSqlDAL coachDAL;
        private ReviewsSqlDAL reviewDAL;

        private string connectionString;

        public HomeController()
        {
            connectionString = ConfigurationManager.ConnectionStrings["MHMConnectionString"].ConnectionString;
            clientDAL = new ClientSqlDAL(connectionString);
            messagesDAL = new MessagesSqlDAL(connectionString);
            coachDAL = new CoachSqlDAL(connectionString);
            reviewDAL = new ReviewsSqlDAL(connectionString);
        }

        //Empty Coach Log-In Page
        [HttpGet]
        public ActionResult CoachLogIn()
        {
            return View();
        }

        //Submission By Coach of Log-In Credentials
        //[HttpPost]
        //public ActionResult CoachLogIn(int id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View("CoachLogIn", id);
        //    }
        //    coachDAL.SaveLogInInfo(savedLogIn);
        //    return RedirectToAction("CoachDash", "Home", new { id = id });
        //}

        //Coach Homepage Upon Successful Log-In
        [HttpGet]
        public ViewResult CoachDash(int id)
        {
            Coach coach = coachDAL.GetCoachDetails(id);
            Reviews reviews = reviewDAL.GetCoachReviews(id);
            ViewBag.Coach = coach;
            ViewBag.Reviews = reviews;
            return View("CoachDash");
        }

        //Empty Client Log-In Page
        [HttpGet]
        public ActionResult ClientLogIn()
        {
            return View();
        }

        //Submission By Client of Log-In Credentials
        //[HttpPost]
        //public ActionResult ClientLogIn(int id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View("ClientLogIn");
        //    }
        //    clientDAL.SaveLogInInfo(savedLogIn);
        //    return RedirectToAction("ClientDash", "Home", new { id = id });
        //}

        //Client Homepage Upon Successful Log-In
        [HttpGet]
        public ViewResult ClientDash(int id)
        {
            Client client = clientDAL.GetClientDetails(id);
            //Surveys surveys = surveyDAL.GetClientSurveys(id);
            //ViewBag.Client = client;
            //ViewBag.Surveys = surveys;
            return View("ClientDash");
        }        
    }
}