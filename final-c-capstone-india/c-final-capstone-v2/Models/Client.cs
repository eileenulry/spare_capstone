﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace c_final_capstone_v2.Models
{
    public class Client
    {
        public int ClientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public string Ethnicity { get; set; }
        public string LocationCity { get; set; }
        public string LocationState { get; set; }
        public string LocationZip { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool SeekingCoach { get; set; }
        public bool VirtualSession { get; set; }
        public string ImageName { get; set; }
        public string AboutMe { get; set; }
        public DateTime ProfileCreated { get; set; }
    }
}