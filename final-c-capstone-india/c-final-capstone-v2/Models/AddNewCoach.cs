﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace c_final_capstone_v2.Models
{
    public class AddNewCoach
    {
        // Placeholder input fields for adding coach information to database

        public int? CoachId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "User Name:")]
        public string Username { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Password:")]
        public string Password { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Passwords do not match")]
        [Display(Name = "Confirm Password:")]
        public string ConfirmPassword { get; set; }


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public string Ethnicity { get; set; }
        public string LocationCity { get; set; }
        public string LocationState { get; set; }
        public string LocationZip { get; set; }
        public string Email { get; set; }
        //public string Password { get; set; }
        public bool SeekingClients { get; set; }
        public bool VirtualSession { get; set; }
        //public string? ImageName { get; set; }
        public string Availability { get; set; }
        public string AboutMe { get; set; }
        public bool IsAdmin { get; set; }
        public DateTime ProfileCreated { get; private set; }

        
        public AddNewCoach()
        {
            ProfileCreated = DateTime.Now.ToLocalTime();
        }
    }
}