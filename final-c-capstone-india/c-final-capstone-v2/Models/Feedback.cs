﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using c_final_capstone_v2;

namespace c_final_capstone_v2.Models
{
    public class Feedback
    {
        public int FeedbackID { get; set; }
        public string ClientFirstName { get; set; }
        public string ClientLastName { get; set; }
        public string CoachFirstName { get; set; }
        public string CoachLastName { get; set; }
        public string Weeks { get; set; }
        public int Question1 { get; set; }
        public int Question2 { get; set; }
        public int Question3 { get; set; }
        public int Question4 { get; set; }
        public int Question5 { get; set; }
        public int Question6 { get; set; }
        public int Question7 { get; set; }
        public string Question8 { get; set; }
        public string Question9 { get; set; }

        public static List<SelectListItem> SessionWeek
        {
            get
            {
                return new List<SelectListItem>
            {
                new SelectListItem {Text = "--", Value = "N/A"},
                new SelectListItem {Text = "Week 1", Value = "1"},
                new SelectListItem {Text = "Week 2", Value = "2"},
                new SelectListItem {Text = "Week 3", Value = "3"},
                new SelectListItem {Text = "Week 4", Value = "4"},
                new SelectListItem {Text = "Week 5", Value = "5"},
                new SelectListItem {Text = "Week 6", Value = "6"},
                new SelectListItem {Text = "Week 7", Value = "7"},
                new SelectListItem {Text = "Week 8", Value = "8"},
                new SelectListItem {Text = "Week 9", Value = "9"},
                new SelectListItem {Text = "Week 10", Value = "10"},
                new SelectListItem {Text = "Week 11", Value = "11"},
                new SelectListItem {Text = "Week 12", Value = "12"},
            };
            }
        }
    }


}