﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace c_final_capstone_v2.Models
{
    public class Reviews
    {
        public int CoachId { get; set; }
        public string CoachFirstName { get; set; }
        public string CoachLastName { get; set; }
        public string SubmitterName { get; set; }
        public string Email { get; set; }
        public string ReviewText { get; set; }
        public DateTime ReviewDate { get; set; }
        public bool IsApproved { get; set; }
        
    }
}