﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace c_final_capstone_v2.Models
{
    public class Conversation
    {
        public string ForUser { get; set; }
        public string WithUser { get; set; }
        public List<Messages> Messages { get; set; } = new List<Messages>();


    }
}