﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System;

namespace c_final_capstone_v2.Models
{
    public class LoginUser
    {
        public int UserId { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsCoach { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public string Ethnicity { get; set; }
        public string LocationCity { get; set; }
        public string LocationState { get; set; }
        public string LocationZip { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "Password:")]
        public string Password { get; set; }
        public bool SeekingCoach { get; set; }
        public bool SeekingClients { get; set; }
        public bool VirtualSession { get; set; }
        public string ImageName { get; set; }
        public string CalendlyLink { get; set; }
        public string AboutMe { get; set; }
        public DateTime ProfileCreated { get; set; }
    }
}