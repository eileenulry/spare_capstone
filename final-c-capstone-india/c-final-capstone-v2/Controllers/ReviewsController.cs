﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using c_final_capstone_v2.Models;
using c_final_capstone_v2.DAL;
using System.Configuration;

namespace c_final_capstone_v2.Controllers
{
    public class ReviewsController : MasterController
    {
        private ReviewsSqlDAL reviewDAL;
        private string connectionString;

        public ReviewsController()
        {
            connectionString = ConfigurationManager.ConnectionStrings["MHMConnectionString"].ConnectionString;
            reviewDAL = new ReviewsSqlDAL(connectionString);

        }
        // GET: Reviews
        public ActionResult CoachReviewsList()
        {
            List<Reviews> reviews = reviewDAL.GetReviews();

            return View("CoachReviewsList", reviews);
        }

        public ActionResult CoachReviewByID(int id)
        {
            Reviews reviews = reviewDAL.GetCoachReviews(id);

            return View("CoachReviewByName", reviews);
        }

        [HttpGet]
        public ActionResult ClientNewReview()
        {
            return View("ClientNewReview"); 
        }

        [HttpPost]
        public ActionResult ClientNewReview(Reviews completedReview)
        {
            if (!ModelState.IsValid)
            {
                return View("ClientNewReview", completedReview);
            }
            else
            {
                reviewDAL.SaveClientReview(completedReview);
                return RedirectToAction("ClientNewReview", "Reviews");
            }
        }
    }
}