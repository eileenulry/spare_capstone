﻿using System;
using System.Linq;
using System.Web;
using System.Configuration;
using c_final_capstone_v2.DAL;
using c_final_capstone_v2.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;

namespace c_final_capstone_v2.Controllers
{
    public class MasterController : Controller
    {
        private const string EmailKey = "Email";
        private const string IsAdminKey = "is_admin";
        private const string IsCoachKey = "is_coach";
        private ClientSqlDAL clientDAL;
        private MessagesSqlDAL messagesDAL;
        private CoachSqlDAL coachDAL;
        private FeedbackSqlDAL feedbackDAL;
        private RegLoginSqlDAL regLoginDAL;
        private string connectionString;

        public MasterController()
        {
            connectionString = ConfigurationManager.ConnectionStrings["MHMConnectionString"].ConnectionString;
            clientDAL = new ClientSqlDAL(connectionString);
            messagesDAL = new MessagesSqlDAL(connectionString);
            coachDAL = new CoachSqlDAL(connectionString);
            feedbackDAL = new FeedbackSqlDAL(connectionString);
            regLoginDAL = new RegLoginSqlDAL(connectionString);
        }

        // Gets the value from the Session
        public string CurrentUser
        {
            get
            {
                string email = string.Empty;

                //Check to see if user session exists, if not create it
                if (Session[EmailKey] != null)
                {
                    email = (string)Session[EmailKey];
                }

                return email;
            }
        }

        // Returns bool if user has authenticated in        
        public bool IsAuthenticated
        {
            get
            {
                return Session[EmailKey] != null;
            }
        }

        // Returns bool if user is an admin    
        public bool IsAdmin
        {
            get
            {
                return Session[IsAdminKey] != null;
            }
        }
        // Returns bool if user is an coach    
        public bool IsCoach
        {
            get
            {
                return Session[IsCoachKey] != null;
            }
        }

        // "Logs" the current user in
        [HttpGet]
        public void LogUserIn(string email, bool isAdmin, bool isCoach)
        {
            Session[EmailKey] = email;
            //Session[PasswordKey] = password;
            if (isAdmin)
            {
                Session[IsAdminKey] = isAdmin;
            }
            else if (isCoach)
            {
                Session[IsCoachKey] = isCoach;
            }
        }

        /// "Logs out" a user by removing the session.
        public void LogUserOut()
        {
            Session.Abandon();
        }

        [ChildActionOnly]
        public ActionResult GetAuthenticatedUser()
        {
            LoginUser model = null;

            if (IsAuthenticated)
            {
                model = regLoginDAL.GetUser(CurrentUser);
            }

            return View("_CoachAuthenticationBar", model);
        }

    }
}