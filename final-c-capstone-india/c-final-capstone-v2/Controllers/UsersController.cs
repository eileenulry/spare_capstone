﻿using c_final_capstone_v2.DAL;
using c_final_capstone_v2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using System.Configuration;


namespace c_final_capstone_v2.Controllers
{
    public class UsersController : MasterController
    {
        private ClientSqlDAL clientDAL;
        private MessagesSqlDAL messagesDAL;
        private CoachSqlDAL coachDAL;
        private ReviewsSqlDAL reviewDAL;
        private RegLoginSqlDAL regLoginDAL;
        private FeedbackSqlDAL feedbackDAL;
        private string connectionString;

        public UsersController()
        {
            connectionString = ConfigurationManager.ConnectionStrings["MHMConnectionString"].ConnectionString;
            clientDAL = new ClientSqlDAL(connectionString);
            messagesDAL = new MessagesSqlDAL(connectionString);
            coachDAL = new CoachSqlDAL(connectionString);
            reviewDAL = new ReviewsSqlDAL(connectionString);
            feedbackDAL = new FeedbackSqlDAL(connectionString);
            regLoginDAL = new RegLoginSqlDAL(connectionString);
        }


        //Log User In
        [HttpGet]
        [Route("login")]
        public ActionResult Login()
        {
            if (base.IsAuthenticated)
            {
                return RedirectToAction("CoachDash", "Home", new { email = base.CurrentUser });
            }

            var model = new LoginUser();

            return View("Login", model);
        }

        [HttpPost]
        [Route("login")]
        public ActionResult Login(LoginUser model)
        {
            if (ModelState.IsValid)
            {
                var user = regLoginDAL.GetUser(model.Email);
                
                if (user == null)
                {
                    ModelState.AddModelError("invalid-user", "The username provided does not exist");
                    return View("Login", model);
                }
                else if (user.Password != model.Password)
                {
                    ModelState.AddModelError("invalid-password", "The password provided is not valid");
                    return View("Login", model);

                }
                return View("Login", model);
            }
            // Happy Path
            base.LogUserIn(model.Email, model.IsAdmin, model.IsCoach);

            if ((model.IsAdmin == false) && (model.IsCoach == false))// then redirect them 
            {
                return RedirectToAction("ClientProfile", "Users", new { id = model.LoginId});
            }
            else if ((model.IsAdmin == false) && (model.IsCoach == true))// then redirect them 
            {
                return RedirectToAction("CoachDash", "Home", new { email = model.LoginId });
            }
            else if (model.IsAdmin == true) // then redirect them 
            {
                return RedirectToAction("CoachProfileList", "Admin", new { email = model.LoginId });
            }
            else
            {
                return View("Login", model);

            }

        }

        // Pulls list of available coaches from DAL
        public ActionResult CoachProfileList()
        {
            List<Coach> coach = coachDAL.FindAvailCoaches();//change back to get avail coaches
            return View("CoachProfileList", coach);
        }

        // Brings up specific coach profile page by Coach ID from DAL
        public ActionResult CoachProfile(int id)
        {
            Coach coach = coachDAL.GetCoachDetails(id);
            return View("CoachProfile", coach);
        }

        //Goes to Coach edit profile screen for Coach selected by ID in DAL
        public ActionResult CoachEditProfile(int id)
        {
            Coach coach = coachDAL.GetCoachDetails(id);
            return View("CoachEditProfile", coach);
        }

        // Sends update statement to database upon client editing information
        [HttpPost]
        public ActionResult CoachEditProfile(Coach coach)
        {
            if (!ModelState.IsValid)
            {
                return View("CoachEditProfile", coach);
            }

            coachDAL.UpdateCoach(coach);
            return RedirectToAction("CoachProfile", "Users", new { id = coach.CoachId });
        }

        // Pulls list of available clients from DAL
        public ActionResult ClientProfileList()
        {
            List<Client> client = clientDAL.FindClients();
            return View("ClientProfileList", client);
        }

        public ActionResult ClientProfile(int id)
        {
            Client newClient = clientDAL.GetClientDetails(id);

            return View("ClientProfile", newClient);
        }

        // Brings up Page for Clients to edit their information
        public ActionResult ClientEditProfile(int id)
        {
            Client client = clientDAL.GetClientDetails(id);
            return View("ClientEditProfile", client);
        }

        // Sends update statement to database upon client editing information
        [HttpPost]
        public ActionResult ClientEditProfile(Client client)
        {
            if (!ModelState.IsValid)
            {
                return View("ClientEditProfile", client);
            }

            clientDAL.UpdateClient(client);
            return RedirectToAction("ClientProfile", "Users", new { id = client.ClientId });
        }

        //Empty Form to Generate a New Client Profile 
        [HttpGet]
        public ActionResult ClientNewProfile()
        {
            return View("ClientNewProfile");
        }

        //Submitting a Completed Client Profile Form
        [HttpPost]
        public ActionResult ClientNewProfile(Client completedClientProfile)
        {
            if (!ModelState.IsValid)
            {
                return View("ClientNewProfile", completedClientProfile);
            }
            else
            {
                clientDAL.SaveClientProfile(completedClientProfile);
                return RedirectToAction("ClientProfileList", "Users");
            }
        }

        //Generates empty client feedback form 
        [HttpGet]
        public ActionResult FeedbackForm()
        {
            return View("FeedbackForm");
        }

        [HttpPost]
        public ActionResult FeedbackForm(Feedback completedFeedback)
        {
            if (!ModelState.IsValid)
            {
                return View("FeedbackForm", completedFeedback);
            }
            else
            {
                feedbackDAL.SaveFeedback(completedFeedback);
                return RedirectToAction("FeedbackForm", "Users");
            }
        }
    }
}