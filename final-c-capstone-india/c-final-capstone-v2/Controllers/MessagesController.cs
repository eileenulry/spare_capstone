﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using c_final_capstone_v2.Models;
using c_final_capstone_v2.DAL;

namespace c_final_capstone_v2.Controllers
{
    public class MessagesController : MasterController
    {
        private ClientSqlDAL clientDAL;
        private MessagesSqlDAL messagesDAL;
        private CoachSqlDAL coachDAL;
        private string connectionString;

        public MessagesController()
        {
            connectionString = ConfigurationManager.ConnectionStrings["MHMConnectionString"].ConnectionString;
            clientDAL = new ClientSqlDAL(connectionString);
            messagesDAL = new MessagesSqlDAL(connectionString);
            coachDAL = new CoachSqlDAL(connectionString);

        }
        [Route("users/ClientProfile/{username}/dashboard")]
        public ActionResult MessageDashboard()
        {
            var messages = messagesDAL.BringInAllMessagesTEST();

            return View("MessageDashboard", messages);
        }

        public ActionResult GetSentMessages(string username)
        {
            var messages = messagesDAL.GetAllSentMessages(username);

            return View("GetSentMessages", messages);
        }
        [Route("users/{forUser}/conversations/{withUser}")]
        public ActionResult GetConversationThread(string forUser, string withUser)
        {
            var conversationThread = messagesDAL.GetConversations(forUser, withUser);
            return View("Conversation", conversationThread);
        }
        [HttpGet]
        public ActionResult NewMessage()
        {
            var model = new Messages();
            return View("NewMessage", model);
        }
        [HttpPost]
        public ActionResult NewMessage(string username, Messages model)
        {
            if (!ModelState.IsValid)
            {
                return View("NewMessage", model);
            }

            var message = new Messages
            {
                MessageText = model.MessageText,
                SenderName = model.SenderName, //will be base.currentUser in final product 
                ReceiverName = model.ReceiverName
            };

            messagesDAL.CreateMessages(message);

            return RedirectToAction("MessageDashboard", "Messages", new { username = base.CurrentUser });
        }

        [HttpGet]
        public ActionResult DeleteMessage(int messageId, string username)
        {
            //var message = messagesDAL.GetAllMessages(messageId);



            return View();
        }

        [HttpPost]
        public ActionResult DeleteMessage(string username, Messages model)
        {
            //var message = messagesDAL.GetAllMessages(model.MessageID);

            //if(message == null)
            //{
            //    return new HttpNotFoundResult();
            //}



            return View();
        }
    }
}