﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using c_final_capstone_v2.Models;
namespace c_final_capstone_v2.DAL
{
    public class RegLoginSqlDAL
    {
        public readonly string connectionString;
        string sql_GetLoginUser = $"SELECT * FROM reglogin WHERE email = @email AND password = @password";
        string sql_GetEmailUser = $"SELECT * FROM reglogin WHERE email = @email";

        //private const string sql_GetLoginUser = $"SELECT * FROM reglogin WHERE email = {email}";


        public RegLoginSqlDAL(string connectionString)
        {
            this.connectionString = connectionString;
        }

        // Pulls in individual user for login
        public LoginUser GetUser(string email)
        {
            LoginUser user = null;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetEmailUser, conn);

                    cmd.Parameters.AddWithValue("@email", email);
                    //cmd.Parameters.AddWithValue("@password", password);


                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        user = MapUserFromReader(reader);
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            return user;
        }

        public LoginUser GetUser(string email, string password)
        {
            LoginUser user = null;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetLoginUser, conn);

                    cmd.Parameters.AddWithValue("@email", email);
                    cmd.Parameters.AddWithValue("@password", password);

                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        user = MapUserFromReader(reader);
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            return user;
        }
        private LoginUser MapUserFromReader(SqlDataReader reader)
        {
            return new LoginUser()
            {
                LoginId = Convert.ToString(reader["login_id"]),
                Email = Convert.ToString(reader["email"]),
                Password = Convert.ToString(reader["password"]),
                AccountType = Convert.ToString(reader["account_type"]),
                AccountCreated = Convert.ToDateTime(reader["acct_created"])
            };



        }
    }
}