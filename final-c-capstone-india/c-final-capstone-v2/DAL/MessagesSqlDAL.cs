﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using c_final_capstone_v2.Models;

namespace c_final_capstone_v2.DAL
{
    public class MessagesSqlDAL
    {
        public string connectionString;
        private const string createNewMessage = @"INSERT INTO message(sender_name, receiver_name, message_text, create_date) VALUES(@SenderName, @ReceiverName, @MessageText, @CreateDate)";
        //private const string getSentMessages = $"SELECT * FROM message WHERE sender_name = '{email}' ORDER BY create_date DESC";
        //private const string getAllMessages = $"SELECT * FROM message WHERE receiver_name = '{email}' ORDER BY create_date DESC";

        public MessagesSqlDAL(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public bool CreateMessages(Messages messages)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(createNewMessage, conn);
                    cmd.Parameters.AddWithValue("@SenderName", messages.SenderName);
                    cmd.Parameters.AddWithValue("@ReceiverName", messages.ReceiverName);
                    cmd.Parameters.AddWithValue("@MessageText", messages.MessageText);
                    cmd.Parameters.AddWithValue("@CreateDate", DateTime.Now);

                    int result = cmd.ExecuteNonQuery();
                    return result > 0;

                }
            }

            catch (SqlException ex)
            {
                throw;
            }
        }

        private void DeleteMessages(int messageId)
        {
            try
            {
                string deleteMessage = $"DELETE FROM message WHERE message_id = " + messageId;

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand();
                    int result = cmd.ExecuteNonQuery();
                }
            }

            catch (SqlException ex)
            {
                throw;
            }
        }

        public List<Messages> GetAllSentMessages(string username)
        {
            List<Messages> getAllSent = new List<Messages>();

            try
            {
                string getSentMessages = $"SELECT * FROM message WHERE sender_name = '{username}' ORDER BY create_date DESC";

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(getSentMessages, conn);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        getAllSent.Add(CreateMessageMap(reader));
                    }

                }
            }

            catch (SqlException ex)
            {
                throw;
            }

            return getAllSent;
        }

        public List<Messages> GetAllMessages(string username)
        {
            List<Messages> getAll = new List<Messages>();

            try
            {
                string getAllMessages = $"SELECT * FROM message WHERE receiver_name = '{username}' ORDER BY create_date DESC";

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(getAllMessages, conn);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        getAll.Add(CreateMessageMap(reader));
                    }

                }
            }

            catch (SqlException ex)
            {
                throw;
            }

            return getAll;
        }

        public List<Messages> BringInAllMessagesTEST()
        {
            List<Messages> getAll = new List<Messages>();

            try
            {
                string getAllMessages = $"SELECT * FROM message ";

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(getAllMessages, conn);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        getAll.Add(CreateMessageMap(reader));
                    }

                }
            }

            catch (SqlException ex)
            {
                throw;
            }

            return getAll;
        }
        private List<Messages> GetMessageThread(string forUser, string withUser, SqlConnection conn)
        {
            List<Messages> output = new List<Messages>();

            string sql = $"SELECT * FROM message WHERE ((sender_name = '{forUser}' and receiver_name = '{withUser}') OR (sender_name = '{withUser}' and receiver_name = '{forUser}')) ORDER BY create_date DESC";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                output.Add(CreateMessageMap(reader));
            }

            return output;
        }
        /// <summary>
        /// Get a conversation thread between two users.
        /// </summary>        
        public Conversation GetConversations(string forUser, string withUser)
        {
            Conversation output = new Conversation()
            {
                ForUser = forUser,
                WithUser = withUser
            };

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    output.Messages = GetMessageThread(forUser, withUser, conn);
                }
            }
            catch (SqlException ex)
            {
                throw;
            }

            return output;
        }

        private Messages CreateMessageMap(SqlDataReader reader)
        {
            return new Messages
            {
                MessageID = Convert.ToInt32(reader["message_id"]),
                SenderName = Convert.ToString(reader["sender_name"]),
                ReceiverName = Convert.ToString(reader["receiver_name"]),
                MessageText = Convert.ToString(reader["message_text"]),
                CreateDate = (Convert.ToDateTime(reader["create_date"]))
            };
        }

    }
}