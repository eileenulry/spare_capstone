﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using c_final_capstone_v2.Models;

namespace c_final_capstone_v2.DAL
{
    public class FeedbackSqlDAL
    {
        private readonly string connectionString;
        private const string SQL_GetFeedback = @"SELECT * FROM feedback ORDER BY coach_last_name";
        private const string SQL_InsertNewFeedback = @"INSERT INTO feedback ([client_first_name], [client_last_name], [coach_first_name], [coach_last_name], [session_week], [question1], [question2], [question3], [question4], [question5], [question6], [question7], [question8], [question9]) VALUES (@ClientFirstName, @ClientLastName, @CoachFirstName, @CoachLastName, @Weeks, @Question1, @Question2, @Question3, @Question4, @Question5, @Question6, @Question7, @Question8, @Question9)";

        public FeedbackSqlDAL(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Feedback> FindFeedback()
        {
            List<Feedback> feedbacks = new List<Feedback>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(SQL_GetFeedback, conn);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Feedback feedback = MapFeedbackFromReader(reader);
                        feedbacks.Add(feedback);
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            return feedbacks;
        }

        public bool SaveFeedback(Feedback newFeedback)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(SQL_InsertNewFeedback, conn);
                    cmd.Parameters.AddWithValue("@ClientFirstName", newFeedback.ClientFirstName);
                    cmd.Parameters.AddWithValue("@ClientLastName", newFeedback.ClientLastName);
                    cmd.Parameters.AddWithValue("@CoachFirstName", newFeedback.CoachFirstName);
                    cmd.Parameters.AddWithValue("@CoachLastName", newFeedback.CoachLastName);
                    cmd.Parameters.AddWithValue("@Weeks", newFeedback.Weeks);
                    cmd.Parameters.AddWithValue("@Question1", newFeedback.Question1);
                    cmd.Parameters.AddWithValue("@Question2", newFeedback.Question2);
                    cmd.Parameters.AddWithValue("@Question3", newFeedback.Question3);
                    cmd.Parameters.AddWithValue("@Question4", newFeedback.Question4);
                    cmd.Parameters.AddWithValue("@Question5", newFeedback.Question5);
                    cmd.Parameters.AddWithValue("@Question6", newFeedback.Question6);
                    cmd.Parameters.AddWithValue("@Question7", newFeedback.Question7);
                    cmd.Parameters.AddWithValue("@Question8", newFeedback.Question8);
                    cmd.Parameters.AddWithValue("@Question9", newFeedback.Question9);

                    int rowsAffected = cmd.ExecuteNonQuery();

                    return rowsAffected > 0;
                }
            }
            catch (SqlException)
            {
                throw;
            }
        }

        private Feedback MapFeedbackFromReader(SqlDataReader reader)
        {
            return new Feedback()
            {
                FeedbackID = Convert.ToInt32(reader["feedback_id"]),
                ClientFirstName = Convert.ToString(reader["client_first_name"]),
                ClientLastName = Convert.ToString(reader["[client_last_name]"]),
                CoachFirstName = Convert.ToString(reader["[coach_first_name]"]),
                CoachLastName = Convert.ToString(reader["[coach_last_name]"]),
                Weeks = Convert.ToString(reader["[session_week]"]),
                Question1 = Convert.ToInt32(reader["[question1]"]),
                Question2 = Convert.ToInt32(reader["[question2]"]),
                Question3 = Convert.ToInt32(reader["[question3]"]),
                Question4 = Convert.ToInt32(reader["[question4]"]),
                Question5 = Convert.ToInt32(reader["[question5]"]),
                Question6 = Convert.ToInt32(reader["[question6]"]),
                Question7 = Convert.ToInt32(reader["[question7]"]),
                Question8 = Convert.ToString(reader["[question8]"]),
                Question9 = Convert.ToString(reader["[question9])"]),
            };
        }
    }
}