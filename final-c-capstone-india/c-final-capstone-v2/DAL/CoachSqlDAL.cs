﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using c_final_capstone_v2.Models;

namespace c_final_capstone_v2.DAL
{
    public class CoachSqlDAL
    {
        private readonly string connectionString;
        private const string getAvailCoaches = @"SELECT * FROM coach ORDER BY seeking_clients DESC"; //to get coaches seeking_clients
        private const string sql_GetAllCoaches = @"SELECT * FROM coach ORDER BY last_name";
        private const string getAllCoachesById = @"SELECT * FROM coach WHERE coach_id = @coach_id"; //for admin to view all coach information last name descending?
        private const string sql_addNewCoach = @"INSERT INTO coach ([first_name], [last_name], [email], [password], [gender], [age], [ethnicity], "
                          + "[location_city], [location_state], [location_zip], [seeking_clients], [virtual_session], [about_me], [is_admin], [profile_created])"
                          + "VALUES (@FirstName, @LastName, @Email, @Password, @Gender, @Age, @Ethnicity, @LocationCity, @LocationState, @LocationZip, @SeekingClients, @VirtualSession, @AboutMe, @IsAdmin, @ProfileCreated)"; //for admin to add new coach and credentials
        //private const string coachReviewsSQL = @"SELECT reviews.submitter_name, reviews.coach_first_name,reviews.coach_last_name, reviews.email, reviews.review_text, coach.first_name, coach.last_name FROM reviews JOIN coach ON reviews.coach_first_name+reviews.coach_last_name=coach.first_name+coach.last_name";
        private string sql_UpdateCoach = @"UPDATE coach "
                          + "SET location_city = @location_city, location_state = @location_state, location_zip = @location_zip, "
                          + "password = @password, seeking_clients = @seeking_clients, virtual_session = @virtual_session, about_me = @about_me "
                          + "WHERE coach_id = @coach_id";
        private string sql_AdminUpdateCoach = @"UPDATE coach "
                          + "SET first_name = @first_name, last_name = @last_name, gender = @gender, age = @age, ethnicity = @ethnicity, "
                          + "location_city = @location_city, location_state = @location_state, location_zip = @location_zip, email = @email, "
                          + "password = @password, seeking_clients = @seeking_clients, virtual_session = @virtual_session, about_me = @about_me, is_admin = @is_admin "
                          + "WHERE coach_id = @coach_id";
        private string sql_AdminRemoveCoach = @"DELETE FROM coach WHERE coach_id = @coach_id";


        public CoachSqlDAL(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public Coach GetCoachDetails(int coachId)
        {
            Coach coachDetails = new Coach();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(getAllCoachesById, conn);
                    cmd.Parameters.AddWithValue("@coach_id", coachId);

                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        coachDetails = MapCoachFromReader(reader);
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            return coachDetails;
        }

        //Creates List of coaches that are seeking clients by reading through database and adding occurences to list to be used in Users Controller
        public List<Coach> FindAvailCoaches()
        {
            List<Coach> availCoaches = new List<Coach>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(getAvailCoaches, conn);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Coach coach = MapCoachFromReader(reader);
                        availCoaches.Add(coach);
                    }
                }
            }
            catch(SqlException ex)
            {
                throw;
            }
            return availCoaches;
        }

        //Creates List of all coaches for admin
        public List<Coach> GetAllCoaches()
        {
            List<Coach> allCoaches = new List<Coach>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(sql_GetAllCoaches, conn);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Coach coach = MapCoachFromReader(reader);
                        allCoaches.Add(coach);
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            return allCoaches;
        }

        //Method to pull in row of CoachReviews by coachid to bind to ReviewsSqlDAL list in UsersController 
        //public List<Coach> GetCoachReviews(string coachName)
        //{
        //    List<Coach> coachReviews = new List<Coach>();

        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(connectionString))
        //        {
        //            conn.Open();

        //            SqlCommand cmd = new SqlCommand(coachReviewsSQL, conn);
        //            cmd.Parameters.AddWithValue("@coach_name", coachName);

        //            SqlDataReader reader = cmd.ExecuteReader();

        //            while (reader.Read())
        //            {
        //                Coach coach = MapCoachFromReader(reader);
        //                coachReviews.Add(coach);
        //            }
        //        }
        //    }
        //    catch (SqlException)
        //    {
        //        throw;
        //    }
        //    return coachReviews;
        //}

        // Updates coach profile information on edit screen
        public bool UpdateCoach(Coach coach)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_UpdateCoach, conn);
                    cmd.Parameters.AddWithValue("@coach_id", coach.CoachId);
                    //cmd.Parameters.AddWithValue("@age", coach.Age);
                    cmd.Parameters.AddWithValue("@location_city", coach.LocationCity);
                    cmd.Parameters.AddWithValue("@location_state", coach.LocationState);
                    cmd.Parameters.AddWithValue("@location_zip", coach.LocationZip);
                    //cmd.Parameters.AddWithValue("@email", coach.Email);
                    cmd.Parameters.AddWithValue("@password", coach.Password);
                    cmd.Parameters.AddWithValue("@seeking_clients", coach.SeekingClients);
                    cmd.Parameters.AddWithValue("@virtual_session", coach.VirtualSession);
                    //cmd.Parameters.AddWithValue("@ImageName", coach.ImageName);
                    cmd.Parameters.AddWithValue("@about_me", coach.AboutMe);

                    int success = cmd.ExecuteNonQuery();

                    if (success > 0)
                    {
                        return true;
                    }
                }
            }
            catch (SqlException ex)
            {
                //throw;
            }
            return false;
        }

        // Allows admin to add a new coach directly to the database. Currently not restricted to just admin
        public bool AdminAddCoach(Coach newCoach)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_addNewCoach, conn);
                    //cmd.Parameters.AddWithValue("@coach_id", newCoach.CoachId);
                    cmd.Parameters.AddWithValue("@FirstName", newCoach.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", newCoach.LastName);
                    cmd.Parameters.AddWithValue("@Email", newCoach.Email);
                    cmd.Parameters.AddWithValue("@Password", newCoach.Password);
                    cmd.Parameters.AddWithValue("@Gender", newCoach.Gender);
                    cmd.Parameters.AddWithValue("@Age", newCoach.Age);
                    cmd.Parameters.AddWithValue("@Ethnicity", newCoach.Ethnicity);
                    cmd.Parameters.AddWithValue("@LocationCity", newCoach.LocationCity);
                    cmd.Parameters.AddWithValue("@LocationState", newCoach.LocationState);
                    cmd.Parameters.AddWithValue("@LocationZip", newCoach.LocationZip);
                    cmd.Parameters.AddWithValue("@SeekingClients", newCoach.SeekingClients);
                    cmd.Parameters.AddWithValue("@VirtualSession", newCoach.VirtualSession);
                    cmd.Parameters.AddWithValue("@AboutMe", newCoach.AboutMe);
                    //cmd.Parameters.AddWithValue("@ImageName", newCoach.ImageName);
                    //cmd.Parameters.AddWithValue("@IsAdmin", newCoach.IsAdmin);
                    cmd.Parameters.AddWithValue("@ProfileCreated", newCoach.ProfileCreated);

                    int rowsAffected = cmd.ExecuteNonQuery();

                    return rowsAffected > 0;
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        // Admin method for updating coach profile information with more options than the coach has available
        public bool AdminUpdateCoach(Coach coach)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_AdminUpdateCoach, conn);
                    cmd.Parameters.AddWithValue("@coach_id", coach.CoachId);
                    cmd.Parameters.AddWithValue("@first_name", coach.FirstName);
                    cmd.Parameters.AddWithValue("@last_name", coach.LastName);
                    cmd.Parameters.AddWithValue("@gender", coach.Gender);
                    cmd.Parameters.AddWithValue("@age", coach.Age);
                    cmd.Parameters.AddWithValue("@ethnicity", coach.Ethnicity);
                    cmd.Parameters.AddWithValue("@location_city", coach.LocationCity);
                    cmd.Parameters.AddWithValue("@location_state", coach.LocationState);
                    cmd.Parameters.AddWithValue("@location_zip", coach.LocationZip);
                    cmd.Parameters.AddWithValue("@email", coach.Email);
                    cmd.Parameters.AddWithValue("@password", coach.Password);
                    cmd.Parameters.AddWithValue("@seeking_clients", coach.SeekingClients);
                    cmd.Parameters.AddWithValue("@virtual_session", coach.VirtualSession);
                    //cmd.Parameters.AddWithValue("@ImageName", coach.ImageName);
                    //cmd.Parameters.AddWithValue("@availability", coach.Availability);
                    cmd.Parameters.AddWithValue("@about_me", coach.AboutMe);
                    //cmd.Parameters.AddWithValue("@is_admin", coach.IsAdmin);
                    cmd.Parameters.AddWithValue("@profile_created", coach.ProfileCreated);

                    int success = cmd.ExecuteNonQuery();

                    if (success > 0)
                    {
                        return true;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            return false;
        }

        // Admin method for deleting coach profile out of database
        public bool AdminRemoveCoach(Coach coach)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_AdminRemoveCoach, conn);
                    cmd.Parameters.AddWithValue("@coach_id", coach.CoachId);

                    int success = cmd.ExecuteNonQuery();

                    if (success > 0)
                    {
                        return true;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            return false;
        }

        //Controller which stores parsed database information from model properties
        private Coach MapCoachFromReader(SqlDataReader reader)
        {
            return new Coach()
            {
                CoachId = Convert.ToInt32(reader["coach_id"]),
                FirstName = Convert.ToString(reader["first_name"]),
                LastName = Convert.ToString(reader["last_name"]),
                Gender = Convert.ToString(reader["gender"]),
                Age = Convert.ToInt32(reader["age"]),
                Ethnicity = Convert.ToString(reader["ethnicity"]),
                Email = Convert.ToString(reader["email"]),
                Password = Convert.ToString(reader["password"]),
                LocationCity = Convert.ToString(reader["location_city"]),
                LocationState = Convert.ToString(reader["location_state"]),
                LocationZip = Convert.ToString(reader["location_zip"]),
                SeekingClients = Convert.ToBoolean(reader["seeking_clients"]),
                VirtualSession = Convert.ToBoolean(reader["virtual_session"]),
                //ImageName = Convert.ToString(reader["image_name"]),
                AboutMe = Convert.ToString(reader["about_me"]),
                //IsAdmin = Convert.ToBoolean(reader["is_admin"]),
                ProfileCreated = Convert.ToDateTime(reader["profile_created"]),
            };
        }
    }
}