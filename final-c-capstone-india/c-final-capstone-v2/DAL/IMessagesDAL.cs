﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using c_final_capstone_v2.Models;

namespace c_final_capstone_v2.DAL
{
    public interface IMessageDAL
    {
        List<Messages> GetMessagesForUser(string username);
        List<Messages> GetAllSentMessagesForUser(string username);

        bool CreateMessages(Messages message);
        Messages GetMessage(int messageId);
        Conversation GetConversations(string forUser, string withUser);

    }
}