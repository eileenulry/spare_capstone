﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using c_final_capstone_v2.Models;

namespace c_final_capstone_v2.DAL
{
    public class ReviewsSqlDAL
    {
        private readonly string connectionString;
        private const string getAllReviews = @"SELECT * FROM reviews";
        private const string coachReviewsSQL = @"SELECT reviews.submitter_name, reviews.coach_first_name,reviews.coach_last_name, reviews.email, reviews.review_text, reviews.review_date, reviews.is_approved, coach.first_name, coach.last_name FROM reviews JOIN coach ON reviews.coach_first_name+reviews.coach_last_name=coach.first_name+coach.last_name";
        private const string insertNewSurvey = @"INSERT INTO reviews ([submitter_name], [coach_first_name], [coach_last_name], [email], [review_text], [review_date], [is_approved])"
                                              + "VALUES (@SubmitterName, @CoachFirstName, @CoachLastName, @Email, @ReviewText, @ReviewDate, @IsApproved)";

        private const string getReviewsForCoach = @"SELECT * FROM reviews WHERE coach_id =@coach_id";

        public ReviewsSqlDAL(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public Reviews GetCoachReviews(int coachId)
        {
            Reviews coachReviews = new Reviews();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(getReviewsForCoach, conn);
                    cmd.Parameters.AddWithValue("@coach_id", coachId);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        coachReviews = MapReviewsFromReader(reader);
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            return coachReviews;
        }

        //Creates List of all reviews for each coachID by reading through database and adding all reviews to list
        public List<Reviews> GetReviews()
        {
            List<Reviews> reviews = new List<Reviews>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(getAllReviews, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Reviews reviewAdd = MapReviewsFromReader(reader);
                        reviews.Add(reviewAdd);
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }

            return reviews;
        }

        public bool SaveClientReview(Reviews newReview)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(insertNewSurvey, conn);
                    cmd.Parameters.AddWithValue("@SubmitterName", newReview.SubmitterName);
                    cmd.Parameters.AddWithValue("@CoachFirstName", newReview.CoachFirstName);
                    cmd.Parameters.AddWithValue("@CoachLastName", newReview.CoachLastName);
                    cmd.Parameters.AddWithValue("@Email", newReview.Email);
                    cmd.Parameters.AddWithValue("@ReviewText", newReview.ReviewText);
                    cmd.Parameters.AddWithValue("@ReviewDate", newReview.ReviewDate);
                    cmd.Parameters.AddWithValue("@IsApproved", newReview.IsApproved);

                    int rowsAffected = cmd.ExecuteNonQuery();

                    return rowsAffected > 0;
                }
            }

            catch (SqlException)
            {
                throw;
            }
        }

        private Reviews MapReviewsFromReader(SqlDataReader reader)
        {
            return new Reviews()
            {
                CoachId = Convert.ToInt32(reader["coach_id"]),
                SubmitterName = Convert.ToString(reader["submitter_name"]),
                CoachFirstName = Convert.ToString(reader["coach_first_name"]),
                CoachLastName = Convert.ToString(reader["coach_last_name"]),
                Email = Convert.ToString(reader["email"]),
                ReviewText = Convert.ToString(reader["review_text"]),
                ReviewDate = Convert.ToDateTime(reader["review_date"]),
                IsApproved = Convert.ToBoolean(reader["is_approved"])
            };
        }
    }
}