﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using c_final_capstone_v2.Models;

namespace c_final_capstone_v2.DAL
{
    public class ClientSqlDAL
    {
        private readonly string connectionString;
        private const string SQL_GetClientProfiles = @"select * from users WHERE seeking_client = 1";
        private string sql_GetByClientId = @"SELECT * FROM users WHERE is_coach = 0 AND user_id = @user_id";
        private const string sql_InsertNewClient = @"INSERT INTO users ([is_coach],[first_name], [last_name], [gender], [age], [ethnicity], [location_city], [location_state], [location_zip], [email], [password], [seeking_coach], [virtual_session], [about_me], [profile_created])"
                                  + "VALUES(@IsCoach, @IsAdmin, @FirstName, @LastName, @Gender, @Age, @Ethnicity, @LocationCity, @LocationState, @LocationZip, @Email, @Password, @SeekingCoach, @SeekingClient, @VirtualSession, @ImageName, @CalendlyLink, @AboutMe, @ProfileCreated)";
        private string sql_UpdateClient = @"UPDATE users "
                                  + "SET is_coach = @is_coach, is_admin = @is_admin, first_name = @first_name, last_name = @last_name, gender = @gender, age = @age, ethnicity = @ethnicity, "
                                  + "location_city = @location_city, location_state = @location_state, location_zip = @location_zip, email = @email, "
                                  + "password = @password, seeking_coach = @seeking_coach, seeking_client = @seeking_client, virtual_session = @virtual_session, image_name = @image_name, calendly_link = @calendly_link, about_me = @about_me "
                                  + "WHERE client_id = @client_id";

        public ClientSqlDAL(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public LoginUser GetClientDetails(int userId)
        {
            LoginUser clientDetails = new LoginUser();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetByClientId, conn);
                    cmd.Parameters.AddWithValue("@user_id", userId);


                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        clientDetails = MapClientFromReader(reader);
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            return clientDetails;
        }

        public List<LoginUser> FindClients()
        {
            List<LoginUser> clients = new List<LoginUser>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(SQL_GetClientProfiles, conn);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        LoginUser client = MapClientFromReader(reader);
                        clients.Add(client);
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            return clients;
        }

        // Save Information Typed Into Profile Form
        public bool SaveClientProfile(LoginUser newClient)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_InsertNewClient, conn);
                    cmd.Parameters.AddWithValue("IsCoach", newClient.IsCoach);
                    cmd.Parameters.AddWithValue("IsAdmin", newClient.IsAdmin);
                    cmd.Parameters.AddWithValue("@FirstName", newClient.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", newClient.LastName);
                    cmd.Parameters.AddWithValue("@Gender", newClient.Gender);
                    cmd.Parameters.AddWithValue("@Age", newClient.Age);
                    cmd.Parameters.AddWithValue("@Ethnicity", newClient.Ethnicity);
                    cmd.Parameters.AddWithValue("@LocationCity", newClient.LocationCity);
                    cmd.Parameters.AddWithValue("@LocationState", newClient.LocationState);
                    cmd.Parameters.AddWithValue("@LocationZip", newClient.LocationZip);
                    cmd.Parameters.AddWithValue("@Email", newClient.Email);
                    cmd.Parameters.AddWithValue("@Password", newClient.Password);
                    cmd.Parameters.AddWithValue("@SeekingCoach", newClient.SeekingCoach);
                    cmd.Parameters.AddWithValue("@SeekingClient", newClient.SeekingClients);
                    cmd.Parameters.AddWithValue("@VirtualSession", newClient.VirtualSession);
                    cmd.Parameters.AddWithValue("@ImageName", newClient.ImageName);
                    cmd.Parameters.AddWithValue("@CalendlyLink", newClient.CalendlyLink);
                    cmd.Parameters.AddWithValue("@AboutMe", newClient.AboutMe);
                    cmd.Parameters.AddWithValue("@ProfileCreated", newClient.ProfileCreated);

                    int rowsAffected = cmd.ExecuteNonQuery();

                    return rowsAffected > 0;

                }
            }
            catch (SqlException)
            {
                throw;
            }
        }


        public bool UpdateClient(LoginUser newUser)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_UpdateClient, conn);
                    cmd.Parameters.AddWithValue("@user_id", newUser.UserId);
                    cmd.Parameters.AddWithValue("@is_admin", newUser.IsAdmin);
                    cmd.Parameters.AddWithValue("@is_coach", newUser.IsCoach);
                    cmd.Parameters.AddWithValue("@first_name", newUser.FirstName);
                    cmd.Parameters.AddWithValue("@last_name", newUser.LastName);
                    cmd.Parameters.AddWithValue("@gender", newUser.Gender);
                    cmd.Parameters.AddWithValue("@age", newUser.Age);
                    cmd.Parameters.AddWithValue("@ethnicity", newUser.Ethnicity);
                    cmd.Parameters.AddWithValue("@location_city", newUser.LocationCity);
                    cmd.Parameters.AddWithValue("@location_state", newUser.LocationState);
                    cmd.Parameters.AddWithValue("@location_zip", newUser.LocationZip);
                    cmd.Parameters.AddWithValue("@email", newUser.Email);
                    cmd.Parameters.AddWithValue("@password", newUser.Password);
                    cmd.Parameters.AddWithValue("@seeking_coach", newUser.SeekingCoach);
                    cmd.Parameters.AddWithValue("@seeking_clients", newUser.SeekingClients);
                    cmd.Parameters.AddWithValue("@virtual_session", newUser.VirtualSession);
                    cmd.Parameters.AddWithValue("@image_name", newUser.ImageName);
                    cmd.Parameters.AddWithValue("@calendly_link", newUser.CalendlyLink);
                    cmd.Parameters.AddWithValue("@about_me", newUser.AboutMe);
                    cmd.Parameters.AddWithValue("@profile_created", newUser.ProfileCreated);

                    int success = cmd.ExecuteNonQuery();

                    if (success > 0)
                    {
                        return true;
                    }
                }
            }

            catch (SqlException ex)
            {
                //throw;
            }
            return false;
        }

        private LoginUser MapClientFromReader(SqlDataReader reader)
        {
            return new LoginUser()
            {
                UserId = Convert.ToInt32(reader["user_id"]),
                IsAdmin = Convert.ToBoolean(reader["is_admin"]),
                IsCoach = Convert.ToBoolean(reader["is_coach"]),
                FirstName = Convert.ToString(reader["first_name"]),
                LastName = Convert.ToString(reader["last_name"]),
                Gender = Convert.ToString(reader["gender"]),
                Age = Convert.ToInt32(reader["age"]),
                Ethnicity = Convert.ToString(reader["ethnicity"]),
                Email = Convert.ToString(reader["email"]),
                Password = Convert.ToString(reader["password"]),
                LocationCity = Convert.ToString(reader["location_city"]),
                LocationState = Convert.ToString(reader["location_state"]),
                LocationZip = Convert.ToString(reader["location_zip"]),
                SeekingCoach = Convert.ToBoolean(reader["seeking_coach"]),
                SeekingClients = Convert.ToBoolean(reader["seeking_clients"]),
                VirtualSession = Convert.ToBoolean(reader["virtual_session"]),
                ImageName = Convert.ToString(reader["image_name"]),
                CalendlyLink = Convert.ToString(reader["about_me"]),
                AboutMe = Convert.ToString(reader["about_me"]),
                ProfileCreated = Convert.ToDateTime(reader["profile_created"]),
            };
        }
    }
}