-- (Drop database in SQL Server)
--USE master
--Drop DATABASE IF EXISTS MHMdb;
--Create DATABASE MHMdb;

--(Drop tables in SQL Server)
USE MHMdb
DROP Table users;
DROP Table message;
DROP Table feedback;
DROP Table reviews; 
DROP Table reglogin;

USE MHMdb 
--Use for Bit values of Client, Admin and coach
CREATE TABLE reglogin (
login_id INT identity(1,1) PRIMARY KEY,
is_admin BIT NOT NULL,
is_coach BIT NOT NULL,
is_client BIT NOT NULL,
email VARCHAR(max) NOT NULL,
password VARCHAR(max) NOT NULL, 
acct_created [datetime] NOT NULL,
);

--RegLogIn add preexisting Admin Acct
INSERT INTO reglogin ([is_admin], [is_coach], [is_client], [email], [password], [acct_created])
VALUES ('1','0','0','adminBill@mhm.org', '123456789', getdate());
INSERT INTO reglogin ([is_admin], [is_coach], [is_client], [email], [password], [acct_created])
VALUES ('1','0','0','admin@mhm.org', 'admin', getdate());

--RegLogIn add preexisting Client Accts
INSERT INTO reglogin ([is_admin], [is_coach], [is_client], [email], [password], [acct_created])
VALUES ('0','0','1','markymark@funkybunch.org', '123456789', getdate());
INSERT INTO reglogin ([is_admin], [is_coach], [is_client], [email], [password], [acct_created])
VALUES ('0','0','1','markysolderbro@funkybunch.org', '123456789', getdate());
INSERT INTO reglogin ([is_admin], [is_coach], [is_client],  [email], [password], [acct_created])
VALUES ('0','0','1','maria@funkybunch.org', '123456789', getdate());
INSERT INTO reglogin ([is_admin], [is_coach], [is_client],[email], [password], [acct_created])
VALUES ('0','0','1','marissa@funkybunch.org', '123456789', getdate());
INSERT INTO reglogin ([is_admin], [is_coach], [is_client],  [email], [password], [acct_created])
VALUES ('0','0','1','parker@funkybunch.org', '123456789', getdate());

--RegLogIn add preexisting Coach Accts
INSERT INTO reglogin ([is_admin], [is_coach], [is_client], [email], [password], [acct_created])
VALUES ('0','1','0','madden@mhm.org','123456789', getdate());
INSERT INTO reglogin ([is_admin], [is_coach], [is_client],  [email], [password], [acct_created])
VALUES ('0','1','0','landry@mhm.org', '123456789', getdate());
INSERT INTO reglogin ([is_admin], [is_coach], [is_client], [email], [password], [acct_created])
VALUES ('0','1','0','lombardi@mhm.org', '123456789', getdate());
INSERT INTO reglogin ([is_admin], [is_coach], [is_client],  [email], [password], [acct_created])
VALUES ('0','1','0','winters@mhm.org','123456789', getdate());
INSERT INTO reglogin ([is_admin], [is_coach], [is_client], [email], [password], [acct_created])
VALUES ('0','1','0','rhodes-smith@mhm.org', '123456789', getdate());


USE MHMdb 

CREATE TABLE users (
	user_id INT identity(1,1) PRIMARY KEY,
	is_coach BIT NULL, 
	is_admin BIT NULL,
	first_name VARCHAR(max) NULL,
	last_name VARCHAR(max) NULL,
	gender VARCHAR(max) NULL,
	age INT NULL,
	ethnicity VARCHAR(max) NULL,
	location_city VARCHAR(max) NULL,
	location_state VARCHAR(max) NULL,
	location_zip VARCHAR(max) NULL,
	email VARCHAR(max) NOT NULL,
	password VARCHAR(max) NOT NULL, 
	seeking_coach BIT NULL,
	seeking_client BIT NULL,
	virtual_session BIT NULL,
	image_name VARCHAR(max) NULL,
	calendly_link VARCHAR(max) NULL,
	about_me  VARCHAR(max) NULL,
	profile_created DATETIME NOT NULL DEFAULT dateadd(hour, 0, GETDATE()),
	);

CREATE TABLE message (
  message_id integer identity NOT NULL,
  sender_name varchar(32) NOT NULL,
  receiver_name varchar(32) NOT NULL,
  message_text varchar(200) NOT NULL,
  create_date DATETIME NOT NULL DEFAULT dateadd(hour, 0, GETDATE()),
  CONSTRAINT pk_message_message_id PRIMARY KEY (message_id)
);

--ALTER TABLE message ADD FOREIGN KEY (sender_name) REFERENCES reglogin(email);
--ALTER TABLE message ADD FOREIGN KEY (receiver_name) REFERENCES reglogin(email);

CREATE TABLE feedback (
	feedback_id INT identity(1,1) PRIMARY KEY,
	client_first_name VARCHAR(max) NOT NULL,
	client_last_name VARCHAR(max) NOT NULL,
	coach_first_name VARCHAR(max) NOT NULL,
	coach_last_name VARCHAR(max) NOT NULL,
	session_week VARCHAR(max) NOT NULL,
	question1 INT NOT NULL,
	question2 INT NOT NULL,
	question3 INT NOT NULL,
	question4 INT NOT NULL,
	question5 INT NOT NULL,
	question6 INT NOT NULL,
	question7 INT NOT NULL,
	question8 VARCHAR(max) NOT NULL,
	question9 VARCHAR(max) NOT NULL
	);
	
CREATE TABLE reviews (
coach_id INT identity(1,1) PRIMARY KEY,
submitter_name VARCHAR(max) NOT NULL,
coach_first_name VARCHAR(max) NOT NULL,
coach_last_name VARCHAR(max) NOT NUll,
email VARCHAR(max) NOT NULL,
review_text VARCHAR(max) NOT NULL,
review_date [datetime] NOT NULL,
is_approved BIT NOT NULL,
);


--User Data
INSERT INTO users ([is_coach], [is_admin], [first_name],[last_name],[gender],[age],[ethnicity],[location_city],[location_state],[location_zip],[email],[password],[seeking_coach], [seeking_client], [virtual_session],[image_name],[calendly_link],[about_me],[profile_created])
VALUES ('0','0','Mark','Wahlberg','Male',47,'White','Oakland','California',94577,'markymark@funkybunch.org','123','1','0','1', null, null,'I am a big fan of Transformers, Four Brothers and working out. Say hello to your mother for me.', getdate());
INSERT INTO users ([is_coach], [is_admin], [first_name],[last_name],[gender],[age],[ethnicity],[location_city],[location_state],[location_zip],[email],[password],[seeking_coach], [seeking_client], [virtual_session],[image_name],[calendly_link],[about_me],[profile_created])
VALUES ('0','0','Donnie','Wahlberg','Male',48,'White','La Jolla','California',93867,'markysolderbro@funkybunch.org','123','1','0','1', null, null,'I am Mark Walhbergs less successful older brother.', getdate());
INSERT INTO users ([is_coach], [is_admin], [first_name],[last_name],[gender],[age],[ethnicity],[location_city],[location_state],[location_zip],[email],[password],[seeking_coach], [seeking_client], [virtual_session],[image_name],[calendly_link],[about_me],[profile_created])
VALUES ('0','0','Maria','Lopez','Female',23,'Hispanic','Bristol','Tennessee',65276,'maria@funkybunch.org','123','1','0','1', null, null,'I love dogs more than cats and ice cream.', getdate());
INSERT INTO users ([is_coach], [is_admin], [first_name],[last_name],[gender],[age],[ethnicity],[location_city],[location_state],[location_zip],[email],[password],[seeking_coach], [seeking_client], [virtual_session],[image_name],[calendly_link],[about_me],[profile_created])
VALUES ('0','0','Marissa','McMichaels','Female',37,'Black','Columbus','Ohio',43123,'marissa@funkybunch.org','123','1','0','1', null, null,'I married the love of my life and have two beautiful daughters.', getdate());
INSERT INTO users ([is_coach], [is_admin], [first_name],[last_name],[gender],[age],[ethnicity],[location_city],[location_state],[location_zip],[email],[password],[seeking_coach], [seeking_client], [virtual_session],[image_name],[calendly_link],[about_me],[profile_created])
VALUES ('0','0','Parker','Johnson','Male',76,'White','Portland','Oregon',44876,'parker@funkybunch.org','123','1','0','1', null, null,'I founded the Funky Bunch and blew all of my money in three years. Please help.', getdate());
INSERT INTO users ([is_coach], [is_admin], [first_name],[last_name],[gender],[age],[ethnicity],[location_city],[location_state],[location_zip],[email],[password],[seeking_coach], [seeking_client], [virtual_session],[image_name],[calendly_link],[about_me],[profile_created])
VALUES ('1','0','John','Madden','Male',82,'White','Columbus','Ohio',43219,'madden@mhm.org','123','0','1','1', null, null,'I am committed to your mental health having experience with anxiety and grief counseling', getdate());
INSERT INTO users ([is_coach], [is_admin], [first_name],[last_name],[gender],[age],[ethnicity],[location_city],[location_state],[location_zip],[email],[password],[seeking_coach], [seeking_client], [virtual_session],[image_name],[calendly_link],[about_me],[profile_created])
VALUES ('1','0','Tom','Landry','Male',54,'Black','Los Angeles','California',94577,'landry@mhm.org','123','0','1','1', null, null,'I am committed to your mental health having experience with anxiety and grief counseling', getdate());
INSERT INTO users ([is_coach], [is_admin], [first_name],[last_name],[gender],[age],[ethnicity],[location_city],[location_state],[location_zip],[email],[password],[seeking_coach], [seeking_client], [virtual_session],[image_name],[calendly_link],[about_me],[profile_created])
VALUES ('1','0','Vince','Lombardi','Male',31,'White','Seattle','Washington',76347,'lombardi@mhm.org','123','0','1','1', null, null,'I am committed to your mental health having experience with anxiety and grief counseling', getdate());
INSERT INTO users ([is_coach], [is_admin], [first_name],[last_name],[gender],[age],[ethnicity],[location_city],[location_state],[location_zip],[email],[password],[seeking_coach], [seeking_client], [virtual_session],[image_name],[calendly_link],[about_me],[profile_created])
VALUES ('1','0','Jillian','Winters','Female',43,'White','Oakland','California',94577,'winters@mhm.org','123','0','1','1', null, null,'I am committed to your mental health having experience with anxiety and grief counseling', getdate());
INSERT INTO users ([is_coach], [is_admin], [first_name],[last_name],[gender],[age],[ethnicity],[location_city],[location_state],[location_zip],[email],[password],[seeking_coach], [seeking_client], [virtual_session],[image_name],[calendly_link],[about_me],[profile_created])
VALUES ('1','0','Anita','Rhodes-Smith','Female',27,'Hispanic','Knoxville','Tennessee',65423,'rhodes-smith@mhm.org','123','0','1','1', null, null,'I am committed to your mental health having experience with anxiety and grief counseling', getdate());
INSERT INTO users ([is_coach], [is_admin], [first_name],[last_name],[gender],[age],[ethnicity],[location_city],[location_state],[location_zip],[email],[password],[seeking_coach], [seeking_client], [virtual_session],[image_name],[calendly_link],[about_me],[profile_created])
VALUES ('0','1','Frank','Reynolds','Male',65,'Hispanic','Tampa','Florida',68917,'admin@mhm.org','123','0','0','0', null, null,'Im the admin', getdate());





--Review table data
INSERT INTO reviews ([submitter_name], [coach_first_name], [coach_last_name], [email], [review_date], [review_text], [is_approved])
VALUES ('Marissa McMichaels','Jillian','Winters','marissa@funkybunch.org','9.10.18','My drinks give me more support than this coach','false')
INSERT INTO reviews ([submitter_name], [coach_first_name], [coach_last_name], [email], [review_date], [review_text], [is_approved])
VALUES ('Mark Wahlberg', 'John','Madden','markymark@funkybunch.org','10.10.18','My coach is an amazing person, like the father I never had', 'true')
INSERT INTO reviews ([submitter_name], [coach_first_name], [coach_last_name], [email], [review_date], [review_text], [is_approved])
VALUES ('Donnie Wahlberg','Tom','Landry','markysolderbro@funkybunch.org','11.10.18','ABSOLUTELY TERRIBLE I WAS CRYING THE WHOLE TIME','false')
INSERT INTO reviews ([submitter_name], [coach_first_name], [coach_last_name], [email], [review_date], [review_text], [is_approved])
VALUES ('Parker Johnson','Anita','Rhodes-Smith','parker@funkybunch.org','9.9.18','I just need an excuse to give this coach a 4','true')
INSERT INTO reviews ([submitter_name], [coach_first_name], [coach_last_name], [email], [review_date], [review_text], [is_approved])
VALUES ('Maria Lopez','Vince','Lombardi','maria@funkybunch.org','11.11.18','They were pretty meh imo','false')

--Message table data 
INSERT INTO message (sender_name, receiver_name, message_text) VALUES ('markymark@funkybunch.org', 'madden@mhm.org', 'Hey John!');
INSERT INTO message (sender_name, receiver_name, message_text) VALUES ('markysolderbro@funkybunch.org','landry@mhm.org', 'Hey Tom!');
INSERT INTO message (sender_name, receiver_name, message_text) VALUES ('maria@funkybunch.org','lombardi@mhm.org', 'Hey Vince!');
INSERT INTO message (sender_name, receiver_name, message_text) VALUES ('marissa@funkybunch.org','winters@mhm.org', 'Hey Jillian!');
INSERT INTO message (sender_name, receiver_name, message_text) VALUES ('parker@funkybunch.org', 'rhodes-smith@mhm.org', 'Hi Anita!');
INSERT INTO message (sender_name, receiver_name, message_text) VALUES ('madden@mhm.org', 'markymark@funkybunch.org', 'Hi Mark!');
INSERT INTO message (sender_name, receiver_name, message_text) VALUES ('landry@mhm.org','markysolderbro@funkybunch.org', 'Hi Donnie!');
INSERT INTO message (sender_name, receiver_name, message_text) VALUES ('lombardi@mhm.org', 'maria@funkybunch.org', 'Hi Maria!');
INSERT INTO message (sender_name, receiver_name, message_text) VALUES ('winters@mhm.org','marissa@funkybunch.org', 'Hi Marissa!');
INSERT INTO message (sender_name, receiver_name, message_text) VALUES ('rhodes-smith@mhm.org', 'parker@funkybunch.org', 'Hi Parker!');


select * from reglogin
SELECT * FROM reglogin WHERE is_admin = 1 
SELECT * FROM reglogin rl JOIN coach c ON c.email = rl.email WHERE rl.is_coach = 1
SELECT * FROM reglogin rl JOIN client c ON c.email = rl.email WHERE rl.is_client = 1


--Admin table data
INSERT INTO coach ([first_name],[last_name],[email],[password],[is_admin],[profile_created])
VALUES ('William','Johnson','adminBill@mhm.org', '123456789', 'true', getdate());
INSERT INTO coach ([first_name],[last_name],[email],[password],[is_admin],[profile_created])
VALUES ('admin', 'admin', 'admin@mhm.org', 'admin', 'true', getdate());
GO






--INSERT Statements
INSERT INTO client ([first_name], [last_name], [gender], [age], [ethnicity], [location_city], [location_state], [location_zip], [email], [password],  [seeking_coach], [virtual_session], [image_name], [about_me], [profile_created]) VALUES (@first_name, @last_name, @gender, @age, @ethnicity, @location_city, @location_state, @location_zip, @email, @password, @seeking_coach, @virtual_session, @image_name, @about_me, @profile_created)

INSERT INTO coach ([first_name],[last_name],[gender],[age],[ethnicity],[location_city],[location_state],[location_zip],[email],[password],[seeking_clients],[virtual_session],[about_me],[is_admin],[profile_created])

INSERT INTO reglogin([acct_type],[first_name],[last_name],[email],[password],[profile_created]) VALUES (@acct_type, @first_name, @last_name, @email, @password, @profile_created)

--SELECT Statements
SELECT * from client
SELECT * from client ORDER BY seeking_Coach DESC
SELECT * FROM client WHERE client_id = @client_id

SELECT * from coach
SELECT coach_id, first_name, last_name, gender, age, ethnicity, location_city, location_state, location_zip, email, seeking_clients, virtual_session, image_name, about_me, profile_created FROM coach ORDER BY seeking_clients DESC

SELECT reviews.submitter_name, reviews.coach_first_name,reviews.coach_last_name, reviews.email, reviews.review_text, coach.first_name, coach.last_name FROM reviews JOIN coach ON reviews.coach_first_name+reviews.coach_last_name=coach.first_name+coach.last_name





--use for string values of isAdmin, isCoach, isClient
CREATE TABLE reglogin (
login_id INT identity(1,1) PRIMARY KEY,
acct_type VARCHAR(max) NOT NULL,
first_name VARCHAR(max) NULL,
last_name VARCHAR(max) NULL,
email VARCHAR(max) NOT NULL,
password VARCHAR(max) NOT NULL, 
acct_created [datetime] NOT NULL,
);

--RegLogIn add preexisting Admin Acct
INSERT INTO reglogin ([acct_type], [first_name], [last_name], [email], [password], [acct_created])
VALUES ('isAdmin', 'William', 'Johnson', 'adminBill@mhm.org', '123456789', getdate());
INSERT INTO reglogin ([acct_type], [first_name], [last_name], [email], [password], [acct_created])
VALUES ('isAdmin', 'admin', 'admin', 'admin@mhm.org', 'admin', getdate());

--RegLogIn add preexisting Client Accts
INSERT INTO reglogin ([acct_type], [first_name], [last_name], [email], [password], [acct_created])
VALUES ('isClient', 'Mark', 'Wahlberg', 'markymark@funkybunch.org', '123456789', getdate());
INSERT INTO reglogin ([acct_type], [first_name], [last_name], [email], [password], [acct_created])
VALUES ('isClient', 'Donnie', 'Wahlberg', 'markysolderbro@funkybunch.org', '123456789', getdate());
INSERT INTO reglogin ([acct_type], [first_name], [last_name], [email], [password], [acct_created])
VALUES ('isClient', 'Maria', 'Lopez', 'maria@funkybunch.org', '123456789', getdate());
INSERT INTO reglogin ([acct_type], [first_name], [last_name], [email], [password], [acct_created])
VALUES ('isClient', 'Marissa', 'McMichaels', 'marissa@funkybunch.org', '123456789', getdate());
INSERT INTO reglogin ([acct_type], [first_name], [last_name], [email], [password], [acct_created])
VALUES ('isClient', 'Parker', 'Johnson', 'parker@funkybunch.org', '123456789', getdate());

--RegLogIn add preexisting Coach Accts
INSERT INTO reglogin ([acct_type], [first_name], [last_name], [email], [password], [acct_created])
VALUES ('isCoach', 'John', 'Madden', 'madden@mhm.org','123456789', getdate());
INSERT INTO reglogin ([acct_type], [first_name], [last_name], [email], [password], [acct_created])
VALUES ('isCoach', 'Tom', 'Landry', 'landry@mhm.org', '123456789', getdate());
INSERT INTO reglogin ([acct_type], [first_name], [last_name], [email], [password], [acct_created])
VALUES ('isCoach', 'Vince', 'Lombardi', 'lombardi@mhm.org', '123456789', getdate());
INSERT INTO reglogin ([acct_type], [first_name], [last_name], [email], [password], [acct_created])
VALUES ('isCoach', 'Jillian', 'Winters', 'winters@mhm.org','123456789', getdate());
INSERT INTO reglogin ([acct_type], [first_name], [last_name], [email], [password], [acct_created])
VALUES ('isCoach', 'Anita', 'Rhodes-Smith', 'rhodes-smith@mhm.org', '123456789', getdate());






CREATE TABLE coach (
	coach_id INT identity(1,1) PRIMARY KEY,
	first_name VARCHAR(max) NULL,
	last_name VARCHAR(max) NULL,
	gender VARCHAR(max) NULL,
	age INT NULL,
	ethnicity VARCHAR(max) NULL,
	location_city VARCHAR(max) NULL,
	location_state VARCHAR(max) NULL,
	location_zip VARCHAR(max) NULL,
	email VARCHAR(max) NOT NULL,
	password VARCHAR(max) NOT NULL, 
	seeking_clients BIT NULL,
	virtual_session BIT NULL,
	image_name VARCHAR(max) NULL,
	availability VARCHAR(max) NULL,
	about_me  VARCHAR(max) NULL,
	is_admin BIT NOT NULL,
	profile_created [datetime] NULL,
	);
	