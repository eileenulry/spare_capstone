Sprint 1 Objectives - Due by Aug 15th 

Client Profile - Ability to fill out profile to be added to network (post registration/login) - 75% complete as of 8/14
Coach Profile - Ability to fill out profile to be added to network (post login) - 75% complete as of 8/14
		- Text box add to list availability
		- Ratings, reviews, about me
Client Browsing - Ability to browse list of coach profiles - Finished 8/14
Coach Browsing - Ability to browse list of client profiles in which are "seeking_coach" (column listed in client data table) - Finished 8/14 

8/15 updates

Coach List/Client List 

Avatar/Client-Coach Name/Location/Virtual Session


Edit Profile - Coach 


SQL Data input 
	Client Table - built 8/13 
	Coach Table (w/ Admin added) - built 8/13 
	Messages Table - built 8/13
	Feedback Table - built 8/13 
	Survey Table (optional) 

Controllers
	Home - 
	Messages - 
	Users - 
	
Models
	AddNewClient -
	AddNewCoach - 
	ChangePasswordModel -
	Client -
	Coach - 
	LoginModel -
	Messages - 

DALs 
	ClientSqlDAL -
	CoachSqlDAL -
	MessagesSqlDAL -

Views
	Home
		Login -
		ViewCoaches - 
	Messages
	
	Shared
		Layout -
		Error -
	Users
		ClientEditProfile
		ClientProfile 
		CoachEditProfile 
		CoachProfile 
		

	
	